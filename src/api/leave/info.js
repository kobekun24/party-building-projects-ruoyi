import request from '@/utils/request'

// 查询党务请假列表
export function listInfo(query) {
  return request({
    url: '/leave/info/list',
    method: 'get',
    params: query 
  })
}

// 查询党务请假详细
export function getInfo(leaveId) {
  return request({
    url: '/leave/info/' + leaveId,
    method: 'get'
  })
}

// 新增党务请假
export function addInfo(data) {
  return request({
    url: '/leave/info',
    method: 'post',
    data: data
  })
}

// 修改党务请假
export function updateInfo(data) {
  return request({
    url: '/leave/info',
    method: 'put',
    data: data
  })
}

// 删除党务请假
export function delInfo(leaveId) {
  return request({
    url: '/leave/info/' + leaveId,
    method: 'delete'
  })
}
