import request from '@/utils/request'

// 查询请假审核人审批记录列表
export function listApprove(query) {
  return request({
    url: '/leave/approve/list',
    method: 'get',
    params: query
  })
}

// 查询请假审核人审批记录详细
export function getApprove(id) {
  return request({
    url: '/leave/approve/' + id,
    method: 'get'
  })
}

// 新增请假审核人审批记录
export function addApprove(data) {
  return request({
    url: '/leave/approve',
    method: 'post',
    data: data
  })
}

// 修改请假审核人审批记录
export function updateApprove(data) {
  return request({
    url: '/leave/approve',
    method: 'put',
    data: data
  })
}

// 删除请假审核人审批记录
export function delApprove(id) {
  return request({
    url: '/leave/approve/' + id,
    method: 'delete'
  })
}
