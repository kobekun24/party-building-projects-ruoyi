import request from '@/utils/request'

// 查询活动会议通知列表
export function listNotice(query) {
  return request({
    url: '/activity/notice/list',
    method: 'get',
    params: query
  })
}

// 查询活动会议通知详细
export function getNotice(activityId) {
  return request({
    url: '/activity/notice/' + activityId,
    method: 'get'
  })
}

// 新增活动会议通知
export function addNotice(data) {
  return request({
    url: '/activity/notice',
    method: 'post',
    data: data
  })
}

// 修改活动会议通知
export function updateNotice(data) {
  return request({
    url: '/activity/notice',
    method: 'put',
    data: data
  })
}

// 删除活动会议通知
export function delNotice(activityId) {
  return request({
    url: '/activity/notice/' + activityId,
    method: 'delete'
  })
}
