import request from '@/utils/request'

// 查询活动会议通知的人员列表
export function listSign(query) {
  return request({
    url: '/activity/sign/list',
    method: 'get',
    params: query
  })
}

// 查询活动会议通知的人员详细
export function getSign(signId) {
  return request({
    url: '/activity/sign/' + signId,
    method: 'get'
  })
}

// 新增活动会议通知的人员
export function addSign(data) {
  return request({
    url: '/activity/sign',
    method: 'post',
    data: data
  })
}

// 修改活动会议通知的人员
export function updateSign(data) {
  return request({
    url: '/activity/sign',
    method: 'put',
    data: data
  })
}

// 删除活动会议通知的人员
export function delSign(signId) {
  return request({
    url: '/activity/sign/' + signId,
    method: 'delete'
  })
}
