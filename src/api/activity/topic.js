import request from '@/utils/request'

// 查询活动会议主题列表
export function listTopic(query) {
  return request({
    url: '/activity/topic/list',
    method: 'get',
    params: query
  })
}

// 查询活动会议主题详细
export function getTopic(topicId) {
  return request({
    url: '/activity/topic/' + topicId,
    method: 'get'
  })
}

// 新增活动会议主题
export function addTopic(data) {
  return request({
    url: '/activity/topic',
    method: 'post',
    data: data
  })
}

// 修改活动会议主题
export function updateTopic(data) {
  return request({
    url: '/activity/topic',
    method: 'put',
    data: data
  })
}

// 删除活动会议主题
export function delTopic(topicId) {
  return request({
    url: '/activity/topic/' + topicId,
    method: 'delete'
  })
}
