import request from '@/utils/request'

// 查询党员类型列表
export function listType(query) {
  return request({
    url: '/member/type/list',
    method: 'get',
    params: query
  })
}
// 查询党员类型下拉列表
export function listTypeOptions(query) {
  return request({
    url: '/member/type/listOptions',
    method: 'get',
    params: query
  })
}
// 查询党员类型详细
export function getType(memberTypeId) {
  return request({
    url: '/member/type/' + memberTypeId,
    method: 'get'
  })
}

// 新增党员类型
export function addType(data) {
  return request({
    url: '/member/type',
    method: 'post',
    data: data
  })
}

// 修改党员类型
export function updateType(data) {
  return request({
    url: '/member/type',
    method: 'put',
    data: data
  })
}

// 删除党员类型
export function delType(memberTypeId) {
  return request({
    url: '/member/type/' + memberTypeId,
    method: 'delete'
  })
}
