import request from '@/utils/request'

// 查询党员信息列表
export function listInfo(data) {
  return request({
    url: '/member/info/list',
    method: 'post',
    data: data
  })
}
// 查询党员信息pop列表
export function listPopInfo(data) {
  return request({
    url: '/member/info/popList',
    method: 'post',
    data: data
  })
}
// 查询党员信息详细
export function getInfo(memberId) {
  return request({
    url: '/member/info/' + memberId,
    method: 'get'
  })
}

// 新增党员信息
export function addInfo(data) {
  return request({
    url: '/member/info',
    method: 'post',
    data: data
  })
}

// 修改党员信息
export function updateInfo(data) {
  return request({
    url: '/member/info',
    method: 'put',
    data: data
  })
}

// 删除党员信息
export function delInfo(memberId) {
  return request({
    url: '/member/info/' + memberId,
    method: 'delete'
  })
}
// 获取可审核党员信息表
export function getListOptions() {
  return request({
    url: '/member/info/listOptions',
    method: 'get'
  })
}