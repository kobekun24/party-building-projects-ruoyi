import request from '@/utils/request'

// 查询党籍状态列表
export function listState(query) {
	return request({
		url: '/member/state/list',
		method: 'get',
		params: query
	})
}
// 查询党籍状态下拉列表
export function listStateOptions(query) {
	return request({
		url: '/member/state/listOptions',
		method: 'get',
		params: query
	})
}
// 查询党籍状态详细
export function getState(memberStateId) {
	return request({
		url: '/member/state/' + memberStateId,
		method: 'get'
	})
}

// 新增党籍状态
export function addState(data) {
	return request({
		url: '/member/state',
		method: 'post',
		data: data
	})
}

// 修改党籍状态
export function updateState(data) {
	return request({
		url: '/member/state',
		method: 'put',
		data: data
	})
}

// 删除党籍状态
export function delState(memberStateId) {
	return request({
		url: '/member/state/' + memberStateId,
		method: 'delete'
	})
}