import request from '@/utils/request'

// 查询党内职务列表
export function listDuty(query) {
  return request({
    url: '/member/duty/list',
    method: 'get',
    params: query
  })
}
// 查询党内职务下拉列表
export function listDutyOptions(query) {
  return request({
    url: '/member/duty/listOptions',
    method: 'get',
    params: query
  })
}
// 查询党内职务详细
export function getDuty(memberDutyId) {
  return request({
    url: '/member/duty/' + memberDutyId,
    method: 'get'
  })
}

// 新增党内职务
export function addDuty(data) {
  return request({
    url: '/member/duty',
    method: 'post',
    data: data
  })
}

// 修改党内职务
export function updateDuty(data) {
  return request({
    url: '/member/duty',
    method: 'put',
    data: data
  })
}

// 删除党内职务
export function delDuty(memberDutyId) {
  return request({
    url: '/member/duty/' + memberDutyId,
    method: 'delete'
  })
}
//配置流程使用
export function baseProcessOptionsList() {
  return request({
    url: '/member/duty/baseProcessOptionsList',
    method: 'get'
  })
}
