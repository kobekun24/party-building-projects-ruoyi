import request from '@/utils/request'

// 查询党员状态记录提醒人配置列表
export function listReminder(query) {
  return request({
    url: '/member/reminder/list',
    method: 'get',
    params: query
  })
}

// 查询党员状态记录提醒人配置详细
export function getReminder(recordReminderId) {
  return request({
    url: '/member/reminder/' + recordReminderId,
    method: 'get'
  })
}

// 新增党员状态记录提醒人配置
export function addReminder(data) {
  return request({
    url: '/member/reminder',
    method: 'post',
    data: data
  })
}

// 修改党员状态记录提醒人配置
export function updateReminder(data) {
  return request({
    url: '/member/reminder',
    method: 'put',
    data: data
  })
}

// 删除党员状态记录提醒人配置
export function delReminder(recordReminderId) {
  return request({
    url: '/member/reminder/' + recordReminderId,
    method: 'delete'
  })
}
