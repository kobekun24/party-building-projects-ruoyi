import request from '@/utils/request'

// 查询党费收支情况列表
export function listBalance(query) {
  return request({
    url: '/fee/balance/list',
    method: 'get',
    params: query
  })
}

// 查询党费收支情况详细
export function getBalance(statBalanceId) {
  return request({
    url: '/fee/balance/' + statBalanceId,
    method: 'get'
  })
}

// 新增党费收支情况
export function addBalance(data) {
  return request({
    url: '/fee/balance',
    method: 'post',
    data: data
  })
}

// 修改党费收支情况
export function updateBalance(data) {
  return request({
    url: '/fee/balance',
    method: 'put',
    data: data
  })
}

// 删除党费收支情况
export function delBalance(statBalanceId) {
  return request({
    url: '/fee/balance/' + statBalanceId,
    method: 'delete'
  })
}
