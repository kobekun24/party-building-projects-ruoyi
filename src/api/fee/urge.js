import request from '@/utils/request'

// 查询党费催交列表
export function listUrge(query) {
  return request({
    url: '/fee/urge/list',
    method: 'get',
    params: query
  })
}

// 查询党费催交详细
export function getUrge(urgeId) {
  return request({
    url: '/fee/urge/' + urgeId,
    method: 'get'
  })
}

// 新增党费催交
export function addUrge(data) {
  return request({
    url: '/fee/urge',
    method: 'post',
    params: data
  })
}

// 修改党费催交
export function updateUrge(data) {
  return request({
    url: '/fee/urge',
    method: 'put',
    data: data
  })
}

// 删除党费催交
export function delUrge(urgeId) {
  return request({
    url: '/fee/urge/' + urgeId,
    method: 'delete'
  })
}
// 一键提醒催交
export function oneClickReminder(data) {
  return request({
    url:'/fee/urge/urgeBatch',
    method: 'post',
    data: data
  })
}

