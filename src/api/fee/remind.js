import request from '@/utils/request'

// 查询党费提醒配置列表
export function listRemind(query) {
  return request({
    url: '/fee/remind/list',
    method: 'get',
    params: query
  })
}

// 获取党费提醒配置详细信息
export function getSetDetail(query) {
  return request({
    url: '/fee/remind/detail',
    method: 'get',
    params: query
  })
}
// 查询党费提醒配置详细
export function getRemind(remindId) {
  return request({
    url: '/fee/remind/' + remindId,
    method: 'get'
  })
}

// 新增党费提醒配置
export function addRemind(data) {
  return request({
    url: '/fee/remind',
    method: 'post',
    data: data
  })
}

// 修改党费提醒配置
export function updateRemind(data) {
  return request({
    url: '/fee/remind',
    method: 'put',
    data: data
  })
}

// 删除党费提醒配置
export function delRemind(remindId) {
  return request({
    url: '/fee/remind/' + remindId,
    method: 'delete'
  })
}
