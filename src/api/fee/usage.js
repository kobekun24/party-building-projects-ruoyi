import request from '@/utils/request'

// 查询党费使用情况列表
export function listUsage(query) {
  return request({
    url: '/fee/usage/list',
    method: 'get',
    params: query
  })
}

// 查询党费使用情况详细
export function getUsage(urgeId) {
  return request({
    url: '/fee/usage/' + urgeId,
    method: 'get'
  })
}

// 新增党费使用情况
export function addUsage(data) {
  return request({
    url: '/fee/usage',
    method: 'post',
    data: data
  })
}

// 修改党费使用情况
export function updateUsage(data) {
  return request({
    url: '/fee/usage',
    method: 'put',
    data: data
  })
}

// 删除党费使用情况
export function delUsage(urgeId) {
  return request({
    url: '/fee/usage/' + urgeId,
    method: 'delete'
  })
}
