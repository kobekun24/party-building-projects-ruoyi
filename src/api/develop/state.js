import request from '@/utils/request'

// 查询发展对象状态列表
export function listState(query) {
  return request({
    url: '/develop/state/list',
    method: 'get',
    params: query
  })
}

// 查询发展对象状态详细
export function getState(targetStateId) {
  return request({
    url: '/develop/state/' + targetStateId,
    method: 'get'
  })
}

// 新增发展对象状态
export function addState(data) {
  return request({
    url: '/develop/state',
    method: 'post',
    data: data
  })
}

// 修改发展对象状态
export function updateState(data) {
  return request({
    url: '/develop/state',
    method: 'put',
    data: data
  })
}

// 删除发展对象状态
export function delState(targetStateId) {
  return request({
    url: '/develop/state/' + targetStateId,
    method: 'delete'
  })
}
// 查询发展对象列表
export function developObjList(query) {
  return request({
    url: '/develop/inspect/list',
    method: 'get',
    params: query
  })
}
// 查看发展历程
export function progress(targetStateId) {
  return request({
    url: 'develop/state/progress/'+targetStateId,
    method: 'get',
  })
}