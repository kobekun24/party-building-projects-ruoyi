import request from '@/utils/request'

// 查询发展对象考察附件列表
export function listFile(query) {
  return request({
    url: '/develop/file/list',
    method: 'get',
    params: query
  })
}

// 查询发展对象考察附件详细
export function getFile(id) {
  return request({
    url: '/develop/file/' + id,
    method: 'get'
  })
}

// 新增发展对象考察附件
export function addFile(data) {
  return request({
    url: '/develop/file',
    method: 'post',
    data: data
  })
}

// 修改发展对象考察附件
export function updateFile(data) {
  return request({
    url: '/develop/file',
    method: 'put',
    data: data
  })
}

// 删除发展对象考察附件
export function delFile(id) {
  return request({
    url: '/develop/file/' + id,
    method: 'delete'
  })
}
