import request from '@/utils/request'

// 查询发展对象考察列表
export function listInspect(query) {
  return request({
    url: '/develop/inspect/list',
    method: 'get',
    params: query
  })
}

// 查询发展对象考察详细
export function getInspect(targetInspectId) {
  return request({
    url: '/develop/inspect/' + targetInspectId,
    method: 'get'
  })
}

// 新增发展对象考察
export function addInspect(data) {
  return request({
    url: '/develop/inspect',
    method: 'post',
    data: data
  })
}

// 修改发展对象考察
export function updateInspect(data) {
  return request({
    url: '/develop/inspect',
    method: 'put',
    data: data
  })
}

// 删除发展对象考察
export function delInspect(targetInspectId) {
  return request({
    url: '/develop/inspect/' + targetInspectId,
    method: 'delete'
  })
}
