import request from '@/utils/request'

// 查询组织关系转入列表
export function listInto(query) {
  return request({
    url: '/relation/into/list',
    method: 'get',
    params: query
  })
}

// 查询组织关系转入详细
export function getInto(transferIntoId) {
  return request({
    url: '/relation/into/' + transferIntoId,
    method: 'get'
  })
}

// 新增组织关系转入
export function addInto(data) {
  return request({
    url: '/relation/into',
    method: 'post',
    data: data
  })
}

// 修改组织关系转入
export function updateInto(data) {
  return request({
    url: '/relation/into',
    method: 'put',
    data: data
  })
}

// 删除组织关系转入
export function delInto(transferIntoId) {
  return request({
    url: '/relation/into/' + transferIntoId,
    method: 'delete'
  })
}
