import request from '@/utils/request'

// 查询组织关系转出列表
export function listOut(query) {
  return request({
    url: '/relation/out/list',
    method: 'get',
    params: query
  })
}

// 查询组织关系转出详细
export function getOut(transferOutId) {
  return request({
    url: '/relation/out/' + transferOutId,
    method: 'get'
  })
}

// 新增组织关系转出
export function addOut(data) {
  return request({
    url: '/relation/out',
    method: 'post',
    data: data
  })
}

// 修改组织关系转出
export function updateOut(data) {
  return request({
    url: '/relation/out',
    method: 'put',
    data: data
  })
}

// 删除组织关系转出
export function delOut(transferOutId) {
  return request({
    url: '/relation/out/' + transferOutId,
    method: 'delete'
  })
}
