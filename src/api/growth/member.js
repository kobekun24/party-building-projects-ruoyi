import request from '@/utils/request'

// 查询发展党员列表
export function listMember(query) {
  return request({
    url: '/growth/member/list',
    method: 'get',
    params: query
  })
}

// 查询发展党员详细
export function getMember(growthMemberId) {
  return request({
    url: '/growth/member/' + growthMemberId,
    method: 'get'
  })
}

// 新增发展党员
export function addMember(data) {
  return request({
    url: '/growth/member',
    method: 'post',
    data: data
  })
}

// 修改发展党员
export function updateMember(data) {
  return request({
    url: '/growth/member',
    method: 'put',
    data: data
  })
}

// 删除发展党员
export function delMember(growthMemberId) {
  return request({
    url: '/growth/member/' + growthMemberId,
    method: 'delete'
  })
}
