import request from '@/utils/request'

// 查询发展党员附件列表
export function listFile(query) {
  return request({
    url: '/growth/file/list',
    method: 'get',
    params: query
  })
}

// 查询发展党员附件详细
export function getFile(id) {
  return request({
    url: '/growth/file/' + id,
    method: 'get'
  })
}

// 新增发展党员附件
export function addFile(data) {
  return request({
    url: '/growth/file',
    method: 'post',
    data: data
  })
}

// 修改发展党员附件
export function updateFile(data) {
  return request({
    url: '/growth/file',
    method: 'put',
    data: data
  })
}

// 删除发展党员附件
export function delFile(id) {
  return request({
    url: '/growth/file/' + id,
    method: 'delete'
  })
}
