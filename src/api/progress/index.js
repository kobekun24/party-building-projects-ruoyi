import request from '@/utils/request'

// 查询党员个人发展历程
export function getDevelopmentHis(userId) {
  return request({
    url: '/system/progress/list/' + userId,
    method: 'get'
  })
}



