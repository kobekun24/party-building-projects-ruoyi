import request from '@/utils/request'

// 查询党员个人考核列表
export function listInfo(query) {
  return request({
    url: '/questionnaire/info/list',
    method: 'get',
    params: query
  })
}

// 查询党员个人考核详细
export function getInfo(id) {
  return request({
    url: '/questionnaire/info/' + id,
    method: 'get'
  })
}

// 新增党员个人考核
export function addInfo(data) {
  return request({
    url: '/questionnaire/info',
    method: 'post',
    data: data
  })
}

// 修改党员个人考核
export function updateInfo(data) {
  return request({
    url: '/questionnaire/info',
    method: 'put',
    data: data
  })
}

// 删除党员个人考核
export function delInfo(id) {
  return request({
    url: '/questionnaire/info/' + id,
    method: 'delete'
  })
}
//个人考核完成情况
export function getTargetList(query) {
  return request({
    url: '/questionnaire/target/list',
    method: 'get',
    params: query
  })
}