import request from '@/utils/request'

// 查询问题选项列表
export function listTargetOption(query) {
  return request({
    url: '/questionnaire/targetOption/list',
    method: 'get',
    params: query
  })
}

// 查询问题选项详细
export function getTargetOption(optionId) {
  return request({
    url: '/questionnaire/targetOption/' + optionId,
    method: 'get'
  })
}

// 新增问题选项
export function addTargetOption(data) {
  return request({
    url: '/questionnaire/targetOption',
    method: 'post',
    data: data
  })
}

// 修改问题选项
export function updateTargetOption(data) {
  return request({
    url: '/questionnaire/targetOption',
    method: 'put',
    data: data
  })
}

// 删除问题选项
export function delTargetOption(optionId) {
  return request({
    url: '/questionnaire/targetOption/' + optionId,
    method: 'delete'
  })
}
