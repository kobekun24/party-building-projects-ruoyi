import request from '@/utils/request'

// 查询考核问卷的问题列表
export function listTargetQuestion(query) {
  return request({
    url: '/questionnaire/targetQuestion/list',
    method: 'get',
    params: query
  })
}

// 查询考核问卷的问题详细
export function getTargetQuestion(questionId) {
  return request({
    url: '/questionnaire/targetQuestion/' + questionId,
    method: 'get'
  })
}

// 新增考核问卷的问题
export function addTargetQuestion(data) {
  return request({
    url: '/questionnaire/targetQuestion',
    method: 'post',
    data: data
  })
}

// 修改考核问卷的问题
export function updateTargetQuestion(data) {
  return request({
    url: '/questionnaire/targetQuestion',
    method: 'put',
    data: data
  })
}

// 删除考核问卷的问题
export function delTargetQuestion(questionId) {
  return request({
    url: '/questionnaire/targetQuestion/' + questionId,
    method: 'delete'
  })
}
