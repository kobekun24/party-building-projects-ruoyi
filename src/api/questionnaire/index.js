import request from '@/utils/request'

// 查询党员个人考核评分指标列表
export function listIndex(query) {
  return request({
    url: '/questionnaire/index/list',
    method: 'get',
    params: query
  })
}

// 查询党员个人考核评分指标详细
export function getIndex(indexId) {
  return request({
    url: '/questionnaire/index/' + indexId,
    method: 'get'
  })
}

// 新增党员个人考核评分指标
export function addIndex(data) {
  return request({
    url: '/questionnaire/index',
    method: 'post',
    data: data
  })
}

// 修改党员个人考核评分指标
export function updateIndex(data) {
  return request({
    url: '/questionnaire/index',
    method: 'put',
    data: data
  })
}

// 删除党员个人考核评分指标
export function delIndex(indexId) {
  return request({
    url: '/questionnaire/index/' + indexId,
    method: 'delete'
  })
}
//获取指标列表
export function selectQuestionnaire(query) {
  return request({
    url: '/questionnaire/index/select',
    method: 'get',
    params: query
  })
}
