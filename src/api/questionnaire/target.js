import request from '@/utils/request'

// 查询党员个人考核记录列表
export function listTarget(query) {
  return request({
    url: '/questionnaire/target/list',
    method: 'get',
    params: query
  })
}

// 查询党员个人考核记录详细
export function getTarget(targetId) {
  return request({
    url: '/questionnaire/target/' + targetId,
    method: 'get'
  })
}

// 新增党员个人考核记录
export function addTarget(data) {
  return request({
    url: '/questionnaire/target',
    method: 'post',
    data: data
  })
}

// 修改党员个人考核记录
export function updateTarget(data) {
  return request({
    url: '/questionnaire/target',
    method: 'put',
    data: data
  })
}

// 删除党员个人考核记录
export function delTarget(targetId) {
  return request({
    url: '/questionnaire/target/' + targetId,
    method: 'delete'
  })
}
