import request from '@/utils/request'

// 查询考核问卷的问题列表
export function listQuestion(query) {
  return request({
    url: '/questionnaire/question/list',
    method: 'get',
    params: query
  })
}

// 查询考核问卷的问题详细
export function getQuestion(questionId) {
  return request({
    url: '/questionnaire/question/' + questionId,
    method: 'get'
  })
}

// 新增考核问卷的问题
export function addQuestion(data) {
  return request({
    url: '/questionnaire/question',
    method: 'post',
    data: data
  })
}

// 修改考核问卷的问题
export function updateQuestion(data) {
  return request({
    url: '/questionnaire/question',
    method: 'put',
    data: data
  })
}

// 删除考核问卷的问题
export function delQuestion(questionId) {
  return request({
    url: '/questionnaire/question/' + questionId,
    method: 'delete'
  })
}
