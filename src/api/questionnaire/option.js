import request from '@/utils/request'

// 查询问题选项列表
export function listOption(query) {
  return request({
    url: '/questionnaire/option/list',
    method: 'get',
    params: query
  })
}

// 查询问题选项详细
export function getOption(optionId) {
  return request({
    url: '/questionnaire/option/' + optionId,
    method: 'get'
  })
}

// 新增问题选项
export function addOption(data) {
  return request({
    url: '/questionnaire/option',
    method: 'post',
    data: data
  })
}

// 修改问题选项
export function updateOption(data) {
  return request({
    url: '/questionnaire/option',
    method: 'put',
    data: data
  })
}

// 删除问题选项
export function delOption(optionId) {
  return request({
    url: '/questionnaire/option/' + optionId,
    method: 'delete'
  })
}
