import request from '@/utils/request'

// 查询特殊党员跟进情况列表
export function listFollow(query) {
  return request({
    url: '/special/follow/list',
    method: 'get',
    params: query
  })
}

// 查询特殊党员跟进情况详细
export function getFollow(specialFollowId) {
  return request({
    url: '/special/follow/' + specialFollowId,
    method: 'get'
  })
}

// 新增特殊党员跟进情况
export function addFollow(data) {
  return request({
    url: '/special/follow',
    method: 'post',
    data: data
  })
}

// 修改特殊党员跟进情况
export function updateFollow(data) {
  return request({
    url: '/special/follow',
    method: 'put',
    data: data
  })
}

// 删除特殊党员跟进情况
export function delFollow(specialFollowId) {
  return request({
    url: '/special/follow/' + specialFollowId,
    method: 'delete'
  })
}
// 查询特殊党员跟进情况详细
export function getInfo(query) {
  return request({
    url: '/special/follow/getInfo' ,
    method: 'get',
    params: query
  })
}