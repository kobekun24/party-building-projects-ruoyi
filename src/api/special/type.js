import request from '@/utils/request'

// 查询特殊党员类型列表
export function listType(query) {
  return request({
    url: '/special/type/list',
    method: 'get',
    params: query
  })
}

// 查询特殊党员类型详细
export function getType(specialTypeId) {
  return request({
    url: '/special/type/' + specialTypeId,
    method: 'get'
  })
}

// 新增特殊党员类型
export function addType(data) {
  return request({
    url: '/special/type',
    method: 'post',
    data: data
  })
}

// 修改特殊党员类型
export function updateType(data) {
  return request({
    url: '/special/type',
    method: 'put',
    data: data
  })
}

// 删除特殊党员类型
export function delType(specialTypeId) {
  return request({
    url: '/special/type/' + specialTypeId,
    method: 'delete'
  })
}
// 查询所有特殊党员类型
export function listAll() {
  return request({
    url: '/special/type/listAll',
    method: 'get'
  })
}
