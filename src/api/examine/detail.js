import request from '@/utils/request'

// 查询审批记录明细列表
export function listDetail(query) {
  return request({
    url: '/examine/detail/list',
    method: 'get',
    params: query
  })
}

// 查询审批记录明细详细
export function getDetail(recordDetailId) {
  return request({
    url: '/examine/detail/' + recordDetailId,
    method: 'get'
  })
}

// 新增审批记录明细
export function addDetail(data) {
  return request({
    url: '/examine/detail',
    method: 'post',
    data: data
  })
}

// 修改审批记录明细
export function updateDetail(data) {
  return request({
    url: '/examine/detail',
    method: 'put',
    data: data
  })
}

// 删除审批记录明细
export function delDetail(recordDetailId) {
  return request({
    url: '/examine/detail/' + recordDetailId,
    method: 'delete'
  })
}

// 审批
export function examineAndApprove(data) {
  return request({
    url: '/examine/record/examine',
    method: 'post',
    data: data
  })
}

//审批详情
export function examineList(data) {
  return request({
    url:'/examine/detail/examineList/',
    method: 'get',
    params:data
  })
}