import request from '@/utils/request'

// 查询审批记录列表
export function listRecord(query) {
  return request({
    url: '/floating/apply/examineList',
    method: 'get',
    params: query
  })
}

// 查询审批记录详细
export function getRecord(recordId) {
  return request({
    url: '/floating/apply/' + recordId,
    method: 'get'
  })
}

// 新增审批记录
export function addRecord(data) {
  return request({
    url: '/floating/apply',
    method: 'post',
    data: data
  })
}

// 修改审批记录
export function updateRecord(data) {
  return request({
    url: '/floating/apply',
    method: 'put',
    data: data
  })
}

// 删除审批记录
export function delRecord(recordId) {
  return request({
    url: '/floating/apply/' + recordId,
    method: 'delete'
  })
}

// 查询审批记录列表
export function examine(query) {
  return request({
    url: '/examine/record/examine',
    method: 'post',
    data: query
  })
}