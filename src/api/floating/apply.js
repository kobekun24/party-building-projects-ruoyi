import request from '@/utils/request'

// 查询组织关系转移申请列表
export function listApply(query) {
  return request({
    url: '/floating/apply/list',
    method: 'get',
    params: query
  })
}
// 查询我的申请列表
export function myList(query) {
  return request({
    url: '/floating/apply/myList',
    method: 'get',
    params: query
  })
}
// 查询组织关系转移申请详细
export function getApply(applyId) {
  return request({
    url: '/floating/apply/' + applyId,
    method: 'get'
  })
}

// 新增组织关系转移申请
export function addApply(data) {
  return request({
    url: '/floating/apply',
    method: 'post',
    data: data
  })
}

// 修改组织关系转移申请
export function updateApply(data) {
  return request({
    url: '/floating/apply',
    method: 'put',
    data: data
  })
}

// 删除组织关系转移申请
export function delApply(applyId) {
  return request({
    url: '/floating/apply/' + applyId,
    method: 'delete'
  })
}
// 销号
export function cancelApply(data) {
  return request({
    url: '/floating/apply/cancel' ,
    method: 'post',
    data: data
  })
}
// 流动管理
export function floatingManager(data) {
  return request({
    url: '/floating/apply/floatingManager' ,
    method: 'post',
    data: data
  })
}

// 查询流动党员审批列表
export function examineList(query) {
  return request({
    url: '/floating/apply/examineList',
    method: 'get',
    params: query
  })
}
// 查询组织关系转移申请列表
export function floatingListtApply(query) {
  return request({
    url: '/floating/apply/floatingList',
    method: 'get',
    params: query
  })
}
//删除情况列表
export function floatingManagerDel(query) {
  return request({
    url: '/floating/apply/floatingManager/'+query,
    method: 'delete',
  })
}
