import request from '@/utils/request'

// 新增活动会议通知
export function addActivityNotice(data) {
  return request({
    url: '/activity/notice',
    method: 'post',
    data: data
  })
}

// 删除活动会议通知
export function delActivityNotice(noticeId) {
  return request({
    url: '/activity/notice/' + noticeId,
    method: 'delete'
  })
}
// 修改活动会议通知
export function updateActivityNotice(data) {
  return request({
    url: '/activity/notice',
    method: 'put',
    data: data
  })
}
// 查询活动会议通知列表
export function getActivityNotice(query) {
  return request({
    url: '/activity/notice/list',
    method: 'get',
	params: query
  })
}
// 签到二维码下载
export function uploadQrCode(query) {
  return request({
    url: '/activity/notice/generate-qrcode',
    method: 'get',
	params: query
  })
}


// 新增活动会议主题
export function addActivityTheme(data) {
  return request({
    url: '/activity/topic',
    method: 'post',
    data: data
  })
}

// 删除活动会议主题
export function delActivityTheme(topicId) {
  return request({
    url: '/activity/topic/' + topicId,
    method: 'delete'
  })
}
// 修改活动会议主题
export function updateActivityTheme(data) {
  return request({
    url: '/activity/topic',
    method: 'put',
    data: data
  })
}
// 查询活动会议通知主题
export function getActivityTheme(query) {
  return request({
    url: '/activity/topic/list',
    method: 'get',
	params: query
  })
}


// 查询活动会议签到列表
export function getSignList(query) {
  return request({
    url: '/activity/sign/list',
    method: 'get',
	params: query
  })
}

// 查询活动报名查询列表
export function getSignUplist(query) {
  return request({
    url: '/activity/sign/signUpList',
    method: 'get',
	params: query
  })
}

// 查询活动报名情况查询列表
export function getUserList(query) {
  return request({
    url: '/activity/sign​/userList',
    method: 'get',
	params: query
  })
}

