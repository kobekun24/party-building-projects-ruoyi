import request from '@/utils/request'

// 查询入党流程配置列表
export function listProcess(query) {
  return request({
    url: '/base/process/list',
    method: 'get',
    params: query
  })
}

// 查询入党流程配置详细
export function getProcess(processId) {
  return request({
    url: '/base/process/' + processId,
    method: 'get'
  })
}

// 新增入党流程配置
export function addProcess(data) {
  return request({
    url: '/base/process',
    method: 'post',
    data: data
  })
}

// 修改入党流程配置
export function updateProcess(data) {
  return request({
    url: '/base/process',
    method: 'put',
    data: data
  })
}

// 删除入党流程配置
export function delProcess(processId) {
  return request({
    url: '/base/process/' + processId,
    method: 'delete'
  })
}
// 查询入党流程树状图
export function getTreeList() {
  return request({
    url: '/base/process/getTreeList',
    method: 'get'
  })
}