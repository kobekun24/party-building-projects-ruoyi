import request from '@/utils/request'

// 查询入党流程配置提醒列表
export function listRemind(query) {
  return request({
    url: '/base/remind/list',
    method: 'get',
    params: query
  })
}

// 查询入党流程配置提醒详细
export function getRemind(processRemindId) {
  return request({
    url: '/base/remind/' + processRemindId,
    method: 'get'
  })
}

// 新增入党流程配置提醒
export function addRemind(data) {
  return request({
    url: '/base/remind',
    method: 'post',
    data: data
  })
}

// 修改入党流程配置提醒
export function updateRemind(data) {
  return request({
    url: '/base/remind',
    method: 'put',
    data: data
  })
}

// 删除入党流程配置提醒
export function delRemind(processRemindId) {
  return request({
    url: '/base/remind/' + processRemindId,
    method: 'delete'
  })
}
