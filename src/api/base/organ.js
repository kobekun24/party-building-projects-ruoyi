import request from '@/utils/request'

// 查询党组织机构列表
export function listOrgan(query) {
  return request({
    url: '/base/organ/list',
    method: 'get',
    params: query
  })
}

// 查询党组织机构详细
export function getOrgan(organId) {
  return request({
    url: '/base/organ/' + organId,
    method: 'get'
  })
}

// 新增党组织机构
export function addOrgan(data) {
  return request({
    url: '/base/organ',
    method: 'post',
    data: data
  })
}

// 修改党组织机构
export function updateOrgan(data) {
  return request({
    url: '/base/organ',
    method: 'put',
    data: data
  })
}

// 删除党组织机构
export function delOrgan(organId) {
  return request({
    url: '/base/organ/' + organId,
    method: 'delete'
  })
}
// 查询党组织机构树
export function getOrganTree() {
  return request({
    // url: '/base/organ/tree',
	url: '/base/organ/myTree',
    method: 'get'
  })
}