import request from '@/utils/request'

// 查询个人党费缴纳情况列表
export function listFee(query) {
  return request({
    url: '/open/fee/list',
    method: 'get',
    params: query
  })
}

// 查询个人党费缴纳情况详细
export function getFee(openFeeId) {
  return request({
    url: '/open/fee/' + openFeeId,
    method: 'get'
  })
}

// 新增个人党费缴纳情况
export function addFee(data) {
  return request({
    url: '/open/fee',
    method: 'post',
    data: data
  })
}

// 修改个人党费缴纳情况
export function updateFee(data) {
  return request({
    url: '/open/fee',
    method: 'put',
    data: data
  })
}

// 删除个人党费缴纳情况
export function delFee(openFeeId) {
  return request({
    url: '/open/fee/' + openFeeId,
    method: 'delete'
  })
}
