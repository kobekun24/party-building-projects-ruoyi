import request from '@/utils/request'

// 查询个人发展历程列表
export function listProgress(query) {
  return request({
    url: '/system/progress/list',
    method: 'get',
    params: query
  })
}

// 查询个人党费缴纳情况详细
export function getProgress(systemProgressId) {
  return request({
    url: '/system/progress/' + systemProgressId,
    method: 'get'
  })
}

// 新增个人党费缴纳情况
export function addProgress(data) {
  return request({
    url: '/system/progress',
    method: 'post',
    data: data
  })
}

// 修改个人党费缴纳情况
export function updateProgress(data) {
  return request({
    url: '/system/progress',
    method: 'put',
    data: data
  })
}

// 删除个人党费缴纳情况
export function delProgress(systemProgressId) {
  return request({
    url: '/system/progress/' + systemProgressId,
    method: 'delete'
  })
}
