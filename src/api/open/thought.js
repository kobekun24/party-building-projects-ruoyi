import request from '@/utils/request'

// 查询个人思想汇报列表
export function listThought(query) {
  return request({
    url: '/open/thought/list',
    method: 'get',
    params: query
  })
}

// 查询个人思想汇报详细
export function getThought(openThoughtId) {
  return request({
    url: '/open/thought/' + openThoughtId,
    method: 'get'
  })
}

// 新增个人思想汇报
export function addThought(data) {
  return request({
    url: '/open/thought',
    method: 'post',
    data: data
  })
}

// 修改个人思想汇报
export function updateThought(data) {
  return request({
    url: '/open/thought',
    method: 'put',
    data: data
  })
}

// 删除个人思想汇报
export function delThought(openThoughtId) {
  return request({
    url: '/open/thought/' + openThoughtId,
    method: 'delete'
  })
}
