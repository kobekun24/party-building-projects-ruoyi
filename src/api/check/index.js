import request from '@/utils/request'

// 查询三会一课工作评价指标列表
export function listIndex(query) {
  return request({
    url: '/check/index/list',
    method: 'get',
    params: query
  })
}

// 查询三会一课工作评价指标详细
export function getIndex(indexId) {
  return request({
    url: '/check/index/' + indexId,
    method: 'get'
  })
}

// 新增三会一课工作评价指标
export function addIndex(data) {
  return request({
    url: '/check/index',
    method: 'post',
    data: data
  })
}

// 修改三会一课工作评价指标
export function updateIndex(data) {
  return request({
    url: '/check/index',
    method: 'put',
    data: data
  })
}

// 删除三会一课工作评价指标
export function delIndex(indexId) {
  return request({
    url: '/check/index/' + indexId,
    method: 'delete'
  })
}
// 三会一课工作评价指标下拉列表
export function getIndexSelect(query) {
  return request({
    url: '/check/index/select',
    method: 'get',
	params: query
  })
}
