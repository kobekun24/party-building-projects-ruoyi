import request from '@/utils/request'

// 查询党支部考核任务列表
export function getTaskList(query) {
	return request({
		url: '/check/task/list',
		method: 'get',
		params: query
	})
}
// 获取党支部考核任务详细信息
export function getTaskDetail(taskId) {
	return request({
		url: '/check/task/' + taskId,
		method: 'get',
	})
}
// 查询党支部考核计划任务的执行组织列表
export function getTaskOrganList(query) {
	return request({
		url: '/check/task/organ/list',
		method: 'get',
		params: query
	})
}
// 打分
export function taskToScore(data) {
	return request({
		url: '/check/task/organ/score',
		method: 'post',
		data: data
	})
}
// 提醒
export function taskRemind(query) {
	return request({
		url: '/check/task/organ/remind',
		method: 'get',
		params: query
	})
}
// 删除考核任务
export function delTask(taskId) {
	return request({
		url: '/check/task/' + taskId,
		method: 'delete'
	})
}
// 佐证材料详情
export function materialDetail(id) {
	return request({
		url: '/check/task/organ/' + id,
		method: 'get'
	})
}
// 支部考核 我的考核
export function getMyAssessment(query) {
	return request({
		url:'/check/task/organ/taskList',
		method: 'get',
		params: query
	})
}
// 保存佐证材料
export function saveMaterialDetail(data) {
	return request({
		url:'/check/task/organ/fileSubmit',
		method: 'post',
		data: data
	})
}
