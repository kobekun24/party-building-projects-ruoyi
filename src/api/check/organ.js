import request from '@/utils/request'

// 查询党支部考核计划任务的执行组织列表
export function listOrgan(query) {
  return request({
    url: '/check/organ/list',
    method: 'get',
    params: query
  })
}

// 查询党支部考核计划任务的执行组织详细
export function getOrgan(id) {
  return request({
    url: '/check/organ/' + id,
    method: 'get'
  })
}

// 新增党支部考核计划任务的执行组织
export function addOrgan(data) {
  return request({
    url: '/check/organ',
    method: 'post',
    data: data
  })
}

// 修改党支部考核计划任务的执行组织
export function updateOrgan(data) {
  return request({
    url: '/check/organ',
    method: 'put',
    data: data
  })
}

// 删除党支部考核计划任务的执行组织
export function delOrgan(id) {
  return request({
    url: '/check/organ/' + id,
    method: 'delete'
  })
}
