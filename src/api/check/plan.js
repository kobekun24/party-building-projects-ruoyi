import request from '@/utils/request'

// 查询党支部考核计划列表
export function listPlan(query) {
  return request({
    url: '/check/plan/list',
    method: 'get',
    params: query
  })
}

// 查询党支部考核计划详细
export function getPlan(planId) {
  return request({
    url: '/check/plan/' + planId,
    method: 'get'
  })
}

// 新增党支部考核计划
export function addPlan(data) {
  return request({
    url: '/check/plan',
    method: 'post',
    data: data
  })
}

// 修改党支部考核计划
export function updatePlan(data) {
  return request({
    url: '/check/plan',
    method: 'put',
    data: data
  })
}

// 删除党支部考核计划
export function delPlan(planId) {
  return request({
    url: '/check/plan/' + planId,
    method: 'delete'
  })
}
