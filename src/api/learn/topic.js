import request from '@/utils/request'

// 查询学习教育培训主题列表
export function listTopic(query) {
  return request({
    url: '/learn/topic/list',
    method: 'get',
    params: query
  })
}

// 查询学习教育培训主题详细
export function getTopic(topicId) {
  return request({
    url: '/learn/topic/' + topicId,
    method: 'get'
  })
}

// 新增学习教育培训主题
export function addTopic(data) {
  return request({
    url: '/learn/topic',
    method: 'post',
    data: data
  })
}

// 修改学习教育培训主题
export function updateTopic(data) {
  return request({
    url: '/learn/topic',
    method: 'put',
    data: data
  })
}

// 删除学习教育培训主题
export function delTopic(topicId) {
  return request({
    url: '/learn/topic/' + topicId,
    method: 'delete'
  })
}
