import request from '@/utils/request'

// 查询学习教育培训通知列表
export function listNotice(query) {
  return request({
    url: '/learn/notice/list',
    method: 'get',
    params: query
  })
}

// 查询学习教育培训通知详细
export function getNotice(noticeId) {
  return request({
    url: '/learn/notice/' + noticeId,
    method: 'get'
  })
}

// 新增学习教育培训通知
export function addNotice(data) {
  return request({
    url: '/learn/notice',
    method: 'post',
    data: data
  })
}

// 修改学习教育培训通知
export function updateNotice(data) {
  return request({
    url: '/learn/notice',
    method: 'put',
    data: data
  })
}

// 删除学习教育培训通知
export function delNotice(noticeId) {
  return request({
    url: '/learn/notice/' + noticeId,
    method: 'delete'
  })
}
// 签到二维码下载
export function uploadQrCode(query) {
  return request({
    url: '/learn/notice/generate-qrcode',
    method: 'get',
	params: query
  })
}

