import request from '@/utils/request'

// 查询培训签到列表
export function listSign(query) {
  return request({
    url: '/learn/sign/list',
    method: 'get',
    params: query
  })
}

// 查询培训签到详细
export function getSign(signId) {
  return request({
    url: '/learn/sign/' + signId,
    method: 'get'
  })
}

// 新增培训签到
export function addSign(data) {
  return request({
    url: '/learn/sign',
    method: 'post',
    data: data
  })
}

// 修改培训签到
export function updateSign(data) {
  return request({
    url: '/learn/sign',
    method: 'put',
    data: data
  })
}

// 删除培训签到
export function delSign(signId) {
  return request({
    url: '/learn/sign/' + signId,
    method: 'delete'
  })
}

//查询学习主题
export function themeList() {
  return request({
    url: 'learn/notice/list',
    method: 'get'
  })
}
