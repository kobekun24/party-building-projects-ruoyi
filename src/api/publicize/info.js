import request from '@/utils/request'

// 查询宣传内容列表
export function listInfo(query) {
  return request({
    url: '/publicize/info/list',
    method: 'get',
    params: query
  })
}

// 查询宣传内容详细
export function getInfo(publicizeId) {
  return request({
    url: '/publicize/info/' + publicizeId,
    method: 'get'
  })
}

// 新增宣传内容
export function addInfo(data) {
  return request({
    url: '/publicize/info',
    method: 'post',
    data: data
  })
}

// 修改宣传内容
export function updateInfo(data) {
  return request({
    url: '/publicize/info',
    method: 'put',
    data: data
  })
}

// 删除宣传内容
export function delInfo(publicizeId) {
  return request({
    url: '/publicize/info/' + publicizeId,
    method: 'delete'
  })
}
