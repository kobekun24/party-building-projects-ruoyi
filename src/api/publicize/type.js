import request from '@/utils/request'

// 查询宣传类型列表
export function listType(query) {
  return request({
    url: '/publicize/type/list',
    method: 'get',
    params: query
  })
}

// 查询宣传类型详细
export function getType(publicizeTypeId) {
  return request({
    url: '/publicize/type/' + publicizeTypeId,
    method: 'get'
  })
}

// 新增宣传类型
export function addType(data) {
  return request({
    url: '/publicize/type',
    method: 'post',
    data: data
  })
}

// 修改宣传类型
export function updateType(data) {
  return request({
    url: '/publicize/type',
    method: 'put',
    data: data
  })
}

// 删除宣传类型
export function delType(publicizeTypeId) {
  return request({
    url: '/publicize/type/' + publicizeTypeId,
    method: 'delete'
  })
}
