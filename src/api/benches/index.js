import request from '@/utils/request'

// 获取工作台上半部分数据
export function getBenchesInfo() {
  return request({
    url: '/work/benches',
    method: 'get',
  })
}
// 工作台活动统计
export function getActivityStatistics(query) {
  return request({
    url: '/work/benches/activity',
    method: 'get',
	params:query
  })
}
// 导出工作台活动统计
export function exportActivityStatistics(data) {
  return request({
    url: '/work/benches/export',
    method: 'post',
	params:data
  })
}
