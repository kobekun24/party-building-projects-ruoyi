import request from '@/utils/request'

// 查询制度建设列表
export function listBuild(query) {
  return request({
    url: '/institution/build/list',
    method: 'get',
    params: query
  })
}

// 查询制度建设详细
export function getBuild(institutionId) {
  return request({
    url: '/institution/build/' + institutionId,
    method: 'get'
  })
}

// 新增制度建设
export function addBuild(data) {
  return request({
    url: '/institution/build',
    method: 'post',
    data: data
  })
}

// 修改制度建设
export function updateBuild(data) {
  return request({
    url: '/institution/build',
    method: 'put',
    data: data
  })
}

// 删除制度建设
export function delBuild(institutionId) {
  return request({
    url: '/institution/build/' + institutionId,
    method: 'delete'
  })
}
