import axios from "axios";
import config from "../config"
export default {
  async get(url: string, data = {}) {
    const logintoken = JSON.parse(localStorage.getItem('loginInfo') as string).token || ""
    return await axios({
      method: 'get',
      url: config.baseURL + url,
      data: data,
      headers: {
        'Authorization': 'Bearer ' + logintoken
      }
    })
  },
  async post(url: string, data = {}) {
    const logintoken = JSON.parse(localStorage.getItem('loginInfo') as string).token || ""
    return await axios({
      method: 'post',
      url: config.baseURL + url,
      data: data,
      headers: {
        'Authorization': 'Bearer ' + logintoken
      }
    });
  }
}