
interface StoreInfo {
  id: number,            //门店id
  name: string,          //门店名称
  status: string,        //
  Photos?: string,       //门店照片  
  longitude: number,     //经度
  latitude: number,      //维度
  phone: string,         //门店电话
  address: string,       //地址描述
  gmt_create: string     //创建时间
  desc: string           //门店描述
}

interface GoodsInfo {
  id: number,             //id
  name: string,           //名称
  status: string,         //状态
  original_price: number, //原价  
  money: number,          //金额
  describe: string        //描述
  tag: string,            //套餐标签

  store_id:number,        //门店id
  phone: string,         //门店电话
  address: string,       //地址描述
  gmt_create: string     //创建时间


}