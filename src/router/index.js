import {
	createWebHistory,
	createRouter,
	createWebHashHistory
} from 'vue-router'
/* Layout */
import Layout from '@/layout'

/**
 * Note: 路由配置项
 *
 * hidden: true                     // 当设置 true 的时候该路由不会再侧边栏出现 如401，login等页面，或者如一些编辑页面/edit/1
 * alwaysShow: true                 // 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
 *                                  // 只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
 *                                  // 若你想不管路由下面的 children 声明的个数都显示你的根路由
 *                                  // 你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
 * redirect: noRedirect             // 当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
 * name:'router-name'               // 设定路由的名字，一定要填写不然使用<keep-alive>时会出现各种问题
 * query: '{"id": 1, "name": "ry"}' // 访问路由的默认传递参数
 * roles: ['admin', 'common']       // 访问路由的角色权限
 * permissions: ['a:a:a', 'b:b:b']  // 访问路由的菜单权限
 * meta : {
    noCache: true                   // 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
    title: 'title'                  // 设置该路由在侧边栏和面包屑中展示的名字
    icon: 'svg-name'                // 设置该路由的图标，对应路径src/assets/icons/svg
    breadcrumb: false               // 如果设置为false，则不会在breadcrumb面包屑中显示
    activeMenu: '/system/user'      // 当路由设置了该属性，则会高亮相对应的侧边栏。
  }
 */

// 公共路由
export const constantRoutes = [{
		path: '/redirect',
		component: Layout,
		hidden: true,
		children: [{
			path: '/redirect/:path(.*)',
			component: () => import('@/views/redirect/index.vue')
		}]
	},
	{
		path: '/login',
		component: () => import('@/views/login'),
		hidden: true
	},
	{
		path: '/register',
		component: () => import('@/views/register'),
		hidden: true
	},
	{
		path: "/:pathMatch(.*)*",
		component: () => import('@/views/error/404'),
		hidden: true
	},
	{
		path: '/401',
		component: () => import('@/views/error/401'),
		hidden: true
	},
	{
		path: '',
		alwaysShow: true,
		component: Layout,
		redirect: '/index',
		hidden: false,
		meta: {
			title: '首页',
			icon: 'house',
			noCache: false
		},
		children: [{
			path: '/index',
			component: () => import('../views/index'),
			name: 'Index',
			meta: {
				title: '工作台',
				icon: '',
				affix: true
			}
		}]
	},
	{
	    path: '/user',
	    component: Layout,
	    hidden: true,
	    redirect: 'noredirect',
	    children: [
	      {
	        path: 'profile',
	        component: () => import('@/views/system/user/profile/index'),
	        name: 'Profile',
	        meta: { title: '个人中心', icon: 'user' }
	      }
	    ]
	  }
	// {
	// 	path: '/learningEducationManagement',
	// 	name: 'LearningEducationManagement',
	// 	alwaysShow: true,
	// 	component: Layout,
	// 	redirect: 'noRedirect',
	// 	hidden: false,
	// 	meta: {
	// 		title: '学习教育管理',
	// 		icon: 'user',
	// 		noCache: false
	// 	},
	// 	children: [{
	// 		name: 'SendTrainingNotifications',
	// 		path: 'sendTrainingNotifications',
	// 		hidden: false,
	// 		component: () => import('../views/learningEducationManagement/sendTrainingNotifications.vue'),
	// 		meta: {
	// 			"title": "发培训通知",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		}
	// 	}, {
	// 		name: 'TrainingAttendance',
	// 		path: 'trainingAttendance',
	// 		hidden: false,
	// 		component: () => import('../views/learningEducationManagement/trainingAttendance.vue'),
	// 		meta: {
	// 			"title": "培训签到",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		}
	// 	}, {
	// 		name: 'LearningThemeManagement',
	// 		path: 'learningThemeManagement',
	// 		hidden: false,
	// 		component: () => import('../views/learningEducationManagement/learningThemeManagement.vue'),
	// 		meta: {
	// 			"title": "学习主题管理",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		}
	// 	}]
	// },
	// {

	// 	path: '/organizationalLifeManagement',
	// 	name: 'OrganizationalLifeManagement',
	// 	alwaysShow: true,
	// 	component: Layout,
	// 	redirect: 'noRedirect',
	// 	hidden: false,
	// 	meta: {
	// 		title: '组织生活管理',
	// 		icon: 'user',
	// 		noCache: false
	// 	},
	// 	children: [{

	// 		name: 'sendEventNotifications',
	// 		path: 'sendEventNotifications',
	// 		hidden: false,
	// 		component: () => import('../views/organizationalLifeManagement/sendEventNotifications.vue'),
	// 		meta: {
	// 			"title": "发活动通知",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		},
	// 	}, {
	// 		name: 'eventRegistration',
	// 		path: 'EventRegistration',
	// 		hidden: false,
	// 		component: () => import('../views/organizationalLifeManagement/eventRegistration.vue'),
	// 		meta: {
	// 			"title": "活动报名",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		},
	// 	}, {
	// 		name: 'eventCheckIn',
	// 		path: 'EventCheckIn',
	// 		hidden: false,
	// 		component: () => import('../views/organizationalLifeManagement/eventCheckIn.vue'),
	// 		meta: {
	// 			"title": "活动签到",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		},
	// 	}, {
	// 		name: 'eventMeetingTypeManagement',
	// 		path: 'EventMeetingTypeManagement',
	// 		hidden: false,
	// 		component: () => import('../views/organizationalLifeManagement/eventMeetingTypeManagement.vue'),
	// 		meta: {
	// 			"title": "活动会议类型管理",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		},

	// 	}, ]
	// },
	// {
	// 	path: '/partyAffairsLeave',
	// 	name: 'PartyAffairsLeave',
	// 	alwaysShow: true,
	// 	component: Layout,
	// 	redirect: 'noRedirect',
	// 	hidden: false,
	// 	meta: {
	// 		title: '党务请假',
	// 		icon: 'user',
	// 		noCache: false
	// 	},
	// 	children: [{
	// 		name: 'PartyMemberLeaveList',
	// 		path: 'partyMemberLeaveList',
	// 		hidden: false,
	// 		component: () => import('../views/partyAffairsLeave/partyMemberLeaveList.vue'),
	// 		meta: {
	// 			"title": "党员请假列表",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		}
	// 	}]

	// }, {
	// 	path: '/partyFeeManagement',
	// 	name: 'PartyFeeManagement',
	// 	alwaysShow: true,
	// 	component: Layout,
	// 	redirect: 'noRedirect',
	// 	hidden: false,
	// 	meta: {
	// 		title: '党费管理',
	// 		icon: 'user',
	// 		noCache: false
	// 	},
	// 	children: [{
	// 		name: 'PaymentStatus',
	// 		path: 'paymentStatus',
	// 		hidden: false,
	// 		component: () => import('../views/partyFeeManagement/paymentStatus.vue'),
	// 		meta: {
	// 			"title": "党员缴费情况",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		}
	// 	}, {
	// 		name: 'IncomeExpenditureSituation',
	// 		path: 'incomeExpenditureSituation',
	// 		hidden: false,
	// 		component: () => import('../views/partyFeeManagement/incomeExpenditureSituation.vue'),
	// 		meta: {
	// 			"title": "党费收支情况",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		}
	// 	}, {
	// 		name: 'Usage',
	// 		path: 'usage',
	// 		hidden: false,
	// 		component: () => import('../views/partyFeeManagement/usage.vue'),
	// 		meta: {
	// 			"title": "党费使用情况",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		}
	// 	}, {
	// 		name: 'ReminderSettings',
	// 		path: 'reminderSettings',
	// 		hidden: false,
	// 		component: () => import('../views/partyFeeManagement/reminderSettings.vue'),
	// 		meta: {
	// 			"title": "缴费提醒设置",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		}
	// 	}]

	// }, {
	// 	path: '/developmentObjectManagement',
	// 	name: 'DevelopmentObjectManagement',
	// 	alwaysShow: true,
	// 	component: Layout,
	// 	redirect: 'noRedirect',
	// 	hidden: false,
	// 	meta: {
	// 		title: '发展对象管理(过程)',
	// 		icon: 'user',
	// 		noCache: false
	// 	},
	// 	children: [{
	// 			name: 'DevelopmentTargets',
	// 			path: 'developmentTargets',
	// 			hidden: false,
	// 			component: () => import('../views/developmentObjectManagement/developmentTargets.vue'),
	// 			meta: {
	// 				"title": "发展对象",
	// 				"icon": "",
	// 				"noCache": false,
	// 				"link": null
	// 			}
	// 		},
	// 		{
	// 			name: 'editorialReviewStatus',
	// 			path: 'editorialReviewStatus',
	// 			hidden: true,
	// 			component: () => import('../views/developmentObjectManagement/editorialReviewStatus.vue'),
	// 			meta: {
	// 				"title": "编辑考察情况",
	// 				"icon": "",
	// 				"noCache": false,
	// 				"link": null,
	// 				"activeMenu": '/developmentObjectManagement/developmentTargets',
	// 			}
	// 		}
	// 	]

	// }, 
	// {
	// 	path: '/developmentPartyMemberManagement',
	// 	name: 'DevelopmentPartyMemberManagement',
	// 	alwaysShow: true,
	// 	component: Layout,
	// 	redirect: 'noRedirect',
	// 	hidden: false,
	// 	meta: {
	// 		title: '发展党员管理',
	// 		icon: 'user',
	// 		noCache: false
	// 	},
	// 	children: [{
	// 		name: 'PartyMemberTraining',
	// 		path: 'partyMemberTraining',
	// 		hidden: false,
	// 		component: () => import('../views/developmentPartyMemberManagement/partyMemberTraining.vue'),
	// 		meta: {
	// 			"title": "党员培养",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		}
	// 	}, {
	// 		name: 'EditInfo',
	// 		path: 'editInfo',
	// 		hidden: true,
	// 		component: () => import('../views/developmentPartyMemberManagement/editInfo.vue'),
	// 		meta: {
	// 			"title": "编辑信息",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null,
	// 			"activeMenu": '/developmentPartyMemberManagement/partyMemberTraining',
	// 		}
	// 	}]

	// }, 
	// {
	// 	path: '/serviceRating',
	// 	name: 'ServiceRating',
	// 	alwaysShow: true,
	// 	component: Layout,
	// 	redirect: 'noRedirect',
	// 	hidden: false,
	// 	meta: {
	// 		title: '党建工作考核评估',
	// 		icon: 'user',
	// 		noCache: false
	// 	},
	// 	children: [{
	// 			name: 'TaskList',
	// 			path: 'taskList',
	// 			hidden: false,
	// 			component: () => import('../views/serviceRating/taskList.vue'),
	// 			meta: {
	// 				"title": "任务列表",
	// 				"icon": "",
	// 				"noCache": false,
	// 				"link": null
	// 			}
	// 		},{
	// 			name: 'TaskDetail',
	// 			path: 'taskDetail',
	// 			hidden: true,
	// 			component: () => import('../views/serviceRating/taskDetail.vue'),
	// 			meta: {
	// 				"title": "任务详情",
	// 				"icon": "",
	// 				"noCache": false,
	// 				"link": null,
	// 				"activeMenu": '/serviceRating/taskList',
	// 			},
	// 		},{
	// 			name: 'MaterialDetail',
	// 			path: 'materialDetail',
	// 			hidden: true,
	// 			component: () => import('../views/serviceRating/materialDetail.vue'),
	// 			meta: {
	// 				"title": "佐证材料",
	// 				"icon": "",
	// 				"noCache": false,
	// 				"link": null,
	// 				"activeMenu": '/serviceRating/taskList',
	// 			},
	// 		},{
	// 			name: 'AssessmentManagement',
	// 			path: 'assessmentManagement',
	// 			hidden: false,
	// 			component: () => import('../views/serviceRating/assessmentManagement.vue'),
	// 			meta: {
	// 				"title": "党支部考核-计划列表",
	// 				"icon": "",
	// 				"noCache": false,
	// 				"link": null
	// 			}
	// 		}, {
	// 			name: 'EditPlan',
	// 			path: 'editPlan',
	// 			hidden: true,
	// 			component: () => import('../views/serviceRating/editPlan.vue'),
	// 			meta: {
	// 				"title": "修改计划",
	// 				"icon": "",
	// 				"noCache": false,
	// 				"link": null,
	// 				"activeMenu": '/serviceRating/assessmentManagement',
	// 			}
	// 		}, {
	// 			name: 'BranchAssessment',
	// 			path: 'branchAssessment',
	// 			hidden: false,
	// 			component: () => import('../views/serviceRating/branchAssessment.vue'),
	// 			meta: {
	// 				"title": "支部考核",
	// 				"icon": "",
	// 				"noCache": false,
	// 				"link": null
	// 			}
	// 		}, {
	// 			name: 'JobEvaluationIndicators',
	// 			path: 'jobEvaluationIndicators',
	// 			hidden: false,
	// 			component: () => import('../views/serviceRating/jobEvaluationIndicators.vue'),
	// 			meta: {
	// 				"title": "三会一课工作评价指标",
	// 				"icon": "",
	// 				"noCache": false,
	// 				"link": null
	// 			}
	// 		}, {
	// 			name: 'UploadMaterial',
	// 			path: 'uploadMaterial',
	// 			hidden: true,
	// 			component: () => import('../views/serviceRating/uploadMaterial.vue'),
	// 			meta: {
	// 				"title": "上传佐证材料",
	// 				"icon": "",
	// 				"noCache": false,
	// 				"link": null,
	// 				"activeMenu": '/serviceRating/branchAssessment',
	// 			}
	// 		}

	// 	]

	// },
	// {
	// 	path: '/personalAssessmentEvaluationManagement',
	// 	name: 'PersonalAssessmentEvaluationManagement',
	// 	alwaysShow: true,
	// 	component: Layout,
	// 	redirect: 'noRedirect',
	// 	hidden: false,
	// 	meta: {
	// 		title: '党员个人考核评价管理',
	// 		icon: 'user',
	// 		noCache: false
	// 	},
	// 	children: [{
	// 		name: 'PartyMemberAssessment',
	// 		path: 'partyMemberAssessment',
	// 		hidden: false,
	// 		component: () => import(
	// 			'../views/personalAssessmentEvaluationManagement/partyMemberAssessment.vue'),
	// 		meta: {
	// 			"title": "党员考核",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		}
	// 	}, {
	// 		name: 'ScoringIndicators',
	// 		path: 'scoringIndicators',
	// 		hidden: false,
	// 		component: () => import(
	// 			'../views/personalAssessmentEvaluationManagement/scoringIndicators.vue'),
	// 		meta: {
	// 			"title": "评分指标",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		}
	// 	}]
	// },
	// {
	// 	path: '/specialPositions',
	// 	name: 'specialPositions',
	// 	alwaysShow: true,
	// 	component: Layout,
	// 	redirect: 'noRedirect',
	// 	hidden: false,
	// 	meta: {
	// 		title: '特殊岗位党建管理',
	// 		icon: 'user',
	// 		noCache: false
	// 	},
	// 	children: [{
	// 			name: 'specialPartyMembers',
	// 			path: 'specialPartyMembers',
	// 			hidden: false,
	// 			component: () => import('../views/specialPositions/specialPartyMembers.vue'),
	// 			meta: {
	// 				"title": "特殊党员",
	// 				"icon": "",
	// 				"noCache": false,
	// 				"link": null
	// 			}
	// 		},
	// 		{
	// 			name: 'specialPartyMembersType',
	// 			path: 'specialPartyMembersType',
	// 			hidden: false,
	// 			component: () => import('../views/specialPositions/specialPartyMembersType.vue'),
	// 			meta: {
	// 				"title": "特殊党员类型管理",
	// 				"icon": "",
	// 				"noCache": false,
	// 				"link": null
	// 			}
	// 		}
	// 	]
	// },
	// {
	// 	path: '/memberInformation',
	// 	name: 'memberInformation',
	// 	alwaysShow: true,
	// 	component: Layout,
	// 	redirect: 'noRedirect',
	// 	hidden: false,
	// 	meta: {
	// 		title: '党员信息公开',
	// 		icon: 'user',
	// 		noCache: false
	// 	},
	// 	children: [{
	// 			name: 'basicPersonalInformation',
	// 			path: 'basicPersonalInformation',
	// 			hidden: false,
	// 			component: () => import('../views/memberInformation/basicPersonalInformation.vue'),
	// 			meta: {
	// 				"title": "党员个人基本信息",
	// 				"icon": "",
	// 				"noCache": false,
	// 				"link": null
	// 			}
	// 		},
	// 		{
	// 			name: 'developmentProcess',
	// 			path: 'developmentProcess',
	// 			hidden: true,
	// 			component: () => import('../views/memberInformation/developmentProcess.vue'),
	// 			meta: {
	// 				"title": "党员个人基本信息",
	// 				"icon": "",
	// 				"noCache": false,
	// 				"link": null,
	// 				"activeMenu": '/memberInformation/basicPersonalInformation',
	// 			}
	// 		}
	// 	]
	// },
	// {
	// 	path: '/organizationalManagementTransfer',
	// 	name: 'organizationalManagementTransfer',
	// 	alwaysShow: true,
	// 	component: Layout,
	// 	redirect: 'noRedirect',
	// 	hidden: false,
	// 	meta: {
	// 		title: '组织关系转移管理',
	// 		icon: 'user',
	// 		noCache: false
	// 	},
	// 	children: [{
	// 			name: 'applicationMembers',
	// 			path: 'applicationMembers',
	// 			hidden: false,
	// 			component: () => import('../views/organizationalManagementTransfer/applicationMembers.vue'),
	// 			meta: {
	// 				"title": "流动党员申请",
	// 				"icon": "",
	// 				"noCache": false,
	// 				"link": null
	// 			}
	// 		},
	// 		{
	// 			name: 'approvalMembers',
	// 			path: 'approvalMembers',
	// 			hidden: false,
	// 			component: () => import('../views/organizationalManagementTransfer/approvalMembers.vue'),
	// 			meta: {
	// 				"title": "流动党员审批",
	// 				"icon": "",
	// 				"noCache": false,
	// 				"link": null
	// 			}
	// 		}, {
	// 			name: 'approval',
	// 			path: 'approval',
	// 			hidden: true,
	// 			component: () => import('../views/organizationalManagementTransfer/approval.vue'),
	// 			meta: {
	// 				"title": "审批详情",
	// 				"icon": "",
	// 				"noCache": false,
	// 				"link": null,
	// 				"activeMenu": '/organizationalManagementTransfer/approvalMembers',
	// 			}
	// 		}, {
	// 			name: 'approvalList',
	// 			path: 'approvalList',
	// 			hidden: true,
	// 			component: () => import('../views/organizationalManagementTransfer/approvalList.vue'),
	// 			meta: {
	// 				"title": "流动党员申请",
	// 				"icon": "",
	// 				"noCache": true,
	// 				"link": null,
	// 				"activeMenu": '/organizationalManagementTransfer/applicationMembers',
	// 			}
	// 		},
	// 		{
	// 			name: 'transferSituation',
	// 			path: 'transferSituation',
	// 			hidden: false,
	// 			component: () => import('../views/organizationalManagementTransfer/transferSituation.vue'),
	// 			meta: {
	// 				"title": "组织管理转移情况",
	// 				"icon": "",
	// 				"noCache": false,
	// 				"link": null
	// 			}
	// 		},
	// 		{
	// 			name: 'situationOfPartyMembers',
	// 			path: 'situationOfPartyMembers',
	// 			hidden: false,
	// 			component: () => import(
	// 				'../views/organizationalManagementTransfer/situationOfPartyMembers.vue'),
	// 			meta: {
	// 				"title": "流动党员情况",
	// 				"icon": "",
	// 				"noCache": false,
	// 				"link": null
	// 			}
	// 		}
	// 	]
	// },
	// {
	// 	path: '/institutionalConstructionManagement',
	// 	name: 'institutionalConstructionManagement',
	// 	alwaysShow: true,
	// 	component: Layout,
	// 	redirect: 'noRedirect',
	// 	hidden: false,
	// 	meta: {
	// 		title: '制度建设管理',
	// 		icon: 'user',
	// 		noCache: false
	// 	},
	// 	children: [{
	// 		name: 'releaseSystem',
	// 		path: 'index',
	// 		hidden: false,
	// 		component: () => import('../views/institutionalConstructionManagement/releaseSystem.vue'),
	// 		meta: {
	// 			"title": "发布制度",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		}
	// 	}]
	// },
	// {
	// 	path: '/publicityOfWork',
	// 	name: 'publicityOfWork',
	// 	alwaysShow: true,
	// 	component: Layout,
	// 	redirect: 'noRedirect',
	// 	hidden: false,
	// 	meta: {
	// 		title: '党建工作宣传',
	// 		icon: 'user',
	// 		noCache: false
	// 	},
	// 	children: [{
	// 			name: 'ReleaseBasedPromotion',
	// 			path: 'releaseBasedPromotion',
	// 			hidden: false,
	// 			component: () => import('../views/publicityOfWork/releaseBasedPromotion.vue'),
	// 			meta: {
	// 				"title": "发布宣传",
	// 				"icon": "",
	// 				"noCache": false,
	// 				"link": null
	// 			}
	// 		}, {
	// 			name: 'Publish',
	// 			path: 'publish',
	// 			hidden: true,
	// 			component: () => import('../views/publicityOfWork/publish.vue'),
	// 			meta: {
	// 				"title": "发布宣传",
	// 				"icon": "",
	// 				"noCache": false,
	// 				"link": null,
	// 				"activeMenu": '/publicityOfWork/releaseBasedPromotion',
	// 			}
	// 		},

	// 		{
	// 			name: 'promotionType',
	// 			path: 'promotionType',
	// 			hidden: false,
	// 			component: () => import('../views/publicityOfWork/promotionType.vue'),
	// 			meta: {
	// 				"title": "宣传类型管理",
	// 				"icon": "",
	// 				"noCache": false,
	// 				"link": null
	// 			}
	// 		},
	// 	]
	// },
	// {
	// 	path: '/partyOrganizationalStructure',
	// 	name: 'partyOrganizationalStructure',
	// 	alwaysShow: true,
	// 	component: Layout,
	// 	redirect: 'noRedirect',
	// 	hidden: false,
	// 	meta: {
	// 		title: '党组织建设管理',
	// 		icon: 'user',
	// 		noCache: false
	// 	},
	// 	children: [{
	// 		name: 'index',
	// 		path: 'index',
	// 		hidden: false,
	// 		component: () => import('../views/partyOrganizationalStructure/index.vue'),
	// 		meta: {
	// 			"title": "党组织机构",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		}
	// 	}]
	// },
	// {
	// 	path: '/partyMemberInformationManagement',
	// 	name: 'PartyMemberInformationManagement',
	// 	alwaysShow: true,
	// 	component: Layout,
	// 	redirect: 'noRedirect',
	// 	hidden: false,
	// 	meta: {
	// 		title: '党员信息管理',
	// 		icon: 'user',
	// 		noCache: false
	// 	},
	// 	children: [{
	// 		name: 'Info',
	// 		path: 'info',
	// 		hidden: false,
	// 		component: () => import('../views/partyMemberInformationManagement/info.vue'),
	// 		meta: {
	// 			"title": "党员信息",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		}
	// 	}, {
	// 		name: 'TypeManagement',
	// 		path: 'typeManagement',
	// 		hidden: false,
	// 		component: () => import('../views/partyMemberInformationManagement/typeManagement.vue'),
	// 		meta: {
	// 			"title": "党员类型管理",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		}
	// 	}, {
	// 		name: 'StateManagement',
	// 		path: 'stateManagement',
	// 		hidden: false,
	// 		component: () => import('../views/partyMemberInformationManagement/stateManagement.vue'),
	// 		meta: {
	// 			"title": "党籍状态管理",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		}
	// 	}, {
	// 		name: 'DutyManagement',
	// 		path: 'dutyManagement',
	// 		hidden: false,
	// 		component: () => import('../views/partyMemberInformationManagement/dutyManagement.vue'),
	// 		meta: {
	// 			"title": "党内职务管理",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		}
	// 	}]
	// },
	// {
	// 	path: '/relationshipManagement',
	// 	name: 'relationshipManagement',
	// 	alwaysShow: true,
	// 	component: Layout,
	// 	redirect: 'noRedirect',
	// 	hidden: false,
	// 	meta: {
	// 		title: '组织关系管理',
	// 		icon: 'user',
	// 		noCache: false
	// 	},
	// 	children: [{
	// 		name: 'transfer',
	// 		path: 'transfer',
	// 		hidden: false,
	// 		component: () => import('../views/relationshipManagement/transfer.vue'),
	// 		meta: {
	// 			"title": "党员转入",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		}
	// 	}, {
	// 		name: 'transferOut',
	// 		path: 'transferOut',
	// 		hidden: false,
	// 		component: () => import('../views/relationshipManagement/transferOut.vue'),
	// 		meta: {
	// 			"title": "党员转出",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null,
	// 		}
	// 	}]
	// },
	// {
	// 	path: '/basicSetting',
	// 	name: 'BasicSetting',
	// 	alwaysShow: true,
	// 	component: Layout,
	// 	redirect: 'noRedirect',
	// 	hidden: false,
	// 	meta: {
	// 		title: '基础设置(用户注册)',
	// 		icon: 'user',
	// 		noCache: false
	// 	},
	// 	children: [{
	// 		"name": "BasicUser",
	// 		"path": "user",
	// 		"hidden": false,
	// 		"component": () => import('../views/system/user/index.vue'),
	// 		"meta": {
	// 			"title": "用户管理",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		}
	// 	}, {
	// 		name: 'ProcessConfiguration',
	// 		path: 'processConfiguration',
	// 		hidden: false,
	// 		component: () => import('../views/basicSetting/processConfiguration.vue'),
	// 		meta: {
	// 			"title": "入党流程配置",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		}
	// 	}, {
	// 		name: 'AddWorkProcess',
	// 		path: 'addWorkProcess',
	// 		hidden: true,
	// 		component: () => import('../views/basicSetting/addWorkProcess.vue'),
	// 		meta: {
	// 			"title": "新增工作事项",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null,
	// 			"activeMenu": '/basicSetting/processConfiguration',
	// 		}
	// 	}, {
	// 		"name": "BasicRole",
	// 		"path": "role",
	// 		"hidden": false,
	// 		"component": () => import('../views/system/role/index.vue'),
	// 		"meta": {
	// 			"title": "角色权限",
	// 			"icon": "",
	// 			"noCache": false,
	// 			"link": null
	// 		}
	// 	}]
	// }

]

// 动态路由，基于用户权限动态去加载
export const dynamicRoutes = [{
		path: '/system/user-auth',
		component: Layout,
		hidden: true,
		permissions: ['system:user:edit'],
		children: [{
			path: 'role/:userId(\\d+)',
			component: () => import('@/views/system/user/authRole'),
			name: 'AuthRole',
			meta: {
				title: '分配角色',
				activeMenu: '/system/user'
			}
		}]
	},
	{
		path: '/system/role-auth',
		component: Layout,
		hidden: true,
		permissions: ['system:role:edit'],
		children: [{
			path: 'user/:roleId(\\d+)',
			component: () => import('@/views/system/role/authUser'),
			name: 'AuthUser',
			meta: {
				title: '分配用户',
				activeMenu: '/system/role'
			}
		}]
	},
	{
		path: '/system/dict-data',
		component: Layout,
		hidden: true,
		permissions: ['system:dict:list'],
		children: [{
			path: 'index/:dictId(\\d+)',
			component: () => import('@/views/system/dict/data'),
			name: 'Data',
			meta: {
				title: '字典数据',
				activeMenu: '/system/dict'
			}
		}]
	},
	{
		path: '/monitor/job-log',
		component: Layout,
		hidden: true,
		permissions: ['monitor:job:list'],
		children: [{
			path: 'index/:jobId(\\d+)',
			component: () => import('@/views/monitor/job/log'),
			name: 'JobLog',
			meta: {
				title: '调度日志',
				activeMenu: '/monitor/job'
			}
		}]
	},
	{
		path: '/tool/gen-edit',
		component: Layout,
		hidden: true,
		permissions: ['tool:gen:edit'],
		children: [{
			path: 'index/:tableId(\\d+)',
			component: () => import('@/views/tool/gen/editTable'),
			name: 'GenEdit',
			meta: {
				title: '修改生成配置',
				activeMenu: '/tool/gen'
			}
		}]
	},
	{
		path: '/serviceRating',
		component: Layout,
		hidden: true,
		permissions: ['check:file:add'],
		children: [{
			path: 'uploadMaterial',
			component: () => import('@/views/serviceRating/uploadMaterial'),
			name: 'uploadMaterial',
			meta: {
				title: '上传佐证材料',
				activeMenu: '/serviceRating'
			}
		}]
	},
	{
		path: '/serviceRating',
		component: Layout,
		hidden: true,
		permissions: ['check:organ:list'],
		children: [{
			path: 'taskDetail',
			component: () => import('@/views/serviceRating/taskDetail'),
			name: 'taskDetail',
			meta: {
				title: '考核任务-查看(详情)',
				activeMenu: '/serviceRating'
			}
		}]
	},
	{
		path: '/serviceRating',
		component: Layout,
		hidden: true,
		permissions: ['check:file:list'],
		children: [{
			path: 'materialDetail',
			component: () => import('@/views/serviceRating/materialDetail'),
			name: 'materialDetail',
			meta: {
				title: '佐证材料',
				activeMenu: '/serviceRating'
			}
		}]
	},
	{
		path: '/serviceRating',
		component: Layout,
		hidden: true,
		permissions: ['check:plan:edit'],
		children: [{
			path: 'editPlan',
			component: () => import('@/views/serviceRating/editPlan'),
			name: 'editPlan',
			meta: {
				title: '修改计划',
				activeMenu: '/serviceRating'
			}
		}]
	},
]

const router = createRouter({
	// history: createWebHistory(),
	history: createWebHashHistory(),
	routes: constantRoutes,
	scrollBehavior(to, from, savedPosition) {
		if (savedPosition) {
			return savedPosition
		} else {
			return {
				top: 0
			}
		}
	},
});

export default router;