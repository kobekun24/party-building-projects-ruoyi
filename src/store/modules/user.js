import {
	login,
	logout,
	getInfo
} from '@/api/login'
import {
	getToken,
	setToken,
	removeToken
} from '@/utils/auth'
import defAva from '@/assets/img/people.png'
const useUserStore = defineStore(
	'user', {
		state: () => ({
			token: getToken(),
			id: '',
			name: '',
			avatar: '',
			roles: [],
			permissions: [],
			// uploadUrl: process.env.NODE_ENV === 'production' ? 'http://27.42.170.198:18421/common/upload' :
			// 	'http://172.16.21.225:8080/common/upload',
			// mutilpleUploadUrl: process.env.NODE_ENV === 'production' ?
			// 	'http://27.42.170.198:18421/common/uploads' : 'http://172.16.21.225:8080/common/uploads',
			uploadUrl: import.meta.env.VITE_APP_BASE_API + 'common/upload',
			mutilpleUploadUrl: import.meta.env.VITE_APP_BASE_API + 'common/uploads'
		}),
		actions: {
			// 登录
			login(userInfo) {
				const username = userInfo.username.trim()
				const password = userInfo.password
				const code = userInfo.code
				const uuid = userInfo.uuid
				return new Promise((resolve, reject) => {
					login(username, password, code, uuid).then(res => {
						setToken(res.token)
						this.token = res.token
						resolve()
					}).catch(error => {
						reject(error)
					})
				})
			},
			// 获取用户信息
			getInfo() {
				return new Promise((resolve, reject) => {
					getInfo().then(res => {
						const user = res.user
						const avatar = (user.avatar == "" || user.avatar == null) ? defAva : user
							.avatar;

						if (res.roles && res.roles.length > 0) { // 验证返回的roles是否是一个非空数组
							this.roles = res.roles
							this.permissions = res.permissions
						} else {
							this.roles = ['ROLE_DEFAULT']
						}
						this.id = user.userId
						this.name = user.nickName
						this.avatar = avatar
						resolve(res)
					}).catch(error => {
						reject(error)
					})
				})
			},
			// 退出系统
			logOut() {
				return new Promise((resolve, reject) => {
					logout(this.token).then(() => {
						this.token = ''
						this.roles = []
						this.permissions = []
						removeToken()
						resolve()
					}).catch(error => {
						reject(error)
					})
				})
			}
		}
	})

export default useUserStore