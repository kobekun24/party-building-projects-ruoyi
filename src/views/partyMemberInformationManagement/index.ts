export interface partyOrganizationTree {
    parentId:number
    id: number
    orderNo:number
    responsibility:string
    activityPlanning:string
    label: string
    createId: number,
    updateId: number,
    children?: partyOrganizationTree[]
  }

